Vue.component('place',{

    props: {
                
     },
    
    template:
        `<div>

        <div class="weather">
                <div id="craglist">Crag list:</div>
                    <select v-model="selectedLocation"  @change="showDay">
                        <option v-for="location of locations" v-bind:value="location">{{ location.name }}</option>
                    </select>
                    <input type="date" v-model="selectedDate" @change="showDay" :max="maxDate(10)" :min="minDate(10)"  />
                    <button id="next" @click="showNextDay">next day</button>
                    <button id="previous" @click="showPreviousDay">previous day</button>
                <div>
                  <p> {{ error }}</p>
                </div>
                <table>
                    <tr>
                        <th>hours</th>
                        <th>temperature</th>
                        <th>precipitations in mm</th>
                        <th>summary</th>
                    </tr>
                    <tr v-for='hf of hourlyWeather.data'>
                        <td> {{ timeStampToHour(hf.time) }}</td>
                        <td> {{ formatTemperature(hf.temperature) }}</td>
                        <td>{{ formatPrecipitations(hf.precipIntensity) }}</td>
                        <td>{{ hf.summary }}</td>
                        <td><img :src=getIcon(hf.icon) width=64px height=64px/></td>
                    </tr>
                </table>
            </div>
        
        
        </div>`,

    data() {
        return {

            hourlyWeather: {},
            baseUrlForecast: 'https://cors.io/?https://api.darksky.net/forecast/646c6f7fb9c163c73f074f29612199d6/',
            error: "",
            selectedLocation: null,
            locations: [
                {
                name: 'misja pec',
                latitude: 45.563962,
                longitude:13.861971,
                id: 0
                }, 
                {
                name: 'korosica',
                latitude: 46.319167,
                longitude:14.599444,
                id: 1
                }   

            ],
            selectedDate: null,
        }
    },
    methods: {

        getWeather(latitude, longitude, time) {
            //make new function to create URL extension
            let url = this.baseUrlForecast+latitude+','+longitude+','+time+'?exclude=daily,flags,alerts&units=ca';
            
            this.$http.get(url).then(response => {
                this.hourlyWeather = response.body.hourly;
            }, 
            response => {
                this.error = response.body.error;
            })
        },

        handleClick() {
            
        },

        showDay() {
            if(this.selectedDate != null && this.selectedLocation != null){
            return this.getWeather(this.selectedLocation.latitude,this.selectedLocation.longitude, this.dateToUnixTimeStamp(this.selectedDate));
            }
        },

        dateToUnixTimeStamp(date){
                           
            let day = new Date(date);
            return Math.round(day.getTime()/1000);
        },

        changeDate(date=null, nbdays, addition=true){
            let day = date ? new Date(date): new Date() ;
            let newDay= null;
            let newDayInMs= null;
            if(addition){
                newDayInMs = day.setDate(day.getDate()+nbdays);
                
            }else{
                newDayInMs = day.setDate(day.getDate()-nbdays);
             
            }
            newDay = new Date(newDayInMs);
            return this.dateToUnixTimeStamp(newDay); 
            
        },
        maxDate(maxDays){
            let today = new Date();
            let maxTS = this.changeDate(date=today, 10);
            return new Date(maxTS * 1000).toISOString().slice(0, 10);
        },
        minDate(maxDays){
            let today = new Date();
            let minTS = this.changeDate(date=today, 10, addition=false);
            return new Date(minTS * 1000).toISOString().slice(0, 10);
        },

        showNextDay(){
            let timeStamp = this.changeDate(this.selectedDate, 1);
            this.selectedDate = new Date(timeStamp * 1000).toISOString().slice(0,10);
            this.getWeather(this.selectedLocation.latitude,this.selectedLocation.longitude, timeStamp);
        },

        showPreviousDay(){
            let timeStamp = this.changeDate(this.selectedDate, 1, addition=false);
            this.selectedDate = new Date(timeStamp * 1000).toISOString().slice(0,10);
            this.getWeather(this.selectedLocation.latitude,this.selectedLocation.longitude,timeStamp)
        },

        timeStampToHour(timeStamp){
            let d = new Date(timeStamp*1000);
            return d.getHours();
        },
        formatTemperature(temperature){
            return Math.round(temperature)+'°C';
        },
        formatPrecipitations(precipitations){
            return Math.round(precipitations).toFixed(1);
        },      
        getIcon(iconId){
            return `./statics/${iconId}.png`;
        }
    },

    computed: {
    
    },
}),

new Vue({

    el: '#app',
    data: {
      
    }, 
    methods: {
      
    },
})


